﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Super_Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the upper limit: ");
            int max = int.Parse(Console.ReadLine());

            for (int i = 0; i * i <= max; i++)
            {
                if (IsPalindrome(i) && IsPalindrome(i * i))
                {
                    Console.WriteLine(i * i);
                }
            }
        }

        static bool IsPalindrome(int num)
        {
            return num == Reverse(num);
        }

        static int Reverse(int num)
        {
            int reverse = 0;

            while (num > 0)
            {
                reverse = (reverse * 10) + (num % 10);
                num /= 10;
            }

            return reverse;
        }
    }
}
